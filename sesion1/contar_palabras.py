#[XXX]
frase = input("Entrega tu frase")
# string
print(frase, type(frase))
# frase -> [palabra1, palabra2, palabra3....]
lista = frase.split()
#
for palabra in lista:
    print(palabra)

cantidad = len(lista)
print(f"Cantidad de palabras {cantidad}")

# caracteres diferentes de la frase:
# set es como lista, pero solamente tiene elementos únicos: es un conjunto
caracteres = set(frase)

print(caracteres)


# conceptos clase 1:

- string, numero
- booleano -> V/F
- if 
- while 
- for
- lista
- set

